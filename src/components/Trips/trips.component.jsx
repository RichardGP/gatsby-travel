import React from 'react'
import * as S from './trips.styles'
import {useStaticQuery, graphql} from "gatsby"
import { Button } from "../Button/button.styles"

import {ImLocation} from "react-icons/im"


const Trips = ({heading}) => {

  const data = useStaticQuery(graphql`
    query TripsQuery {
      allTripsJson {
        edges {
          node {
            alt
            button
            name
            img {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  `)

  function getTrips(data){
    const tripsArray = [];
    data.allTripsJson.edges.forEach((item, index) => {
      tripsArray.push(
        <S.ProductCart key={index}>
          <S.ProductImg src={item.node.img.childImageSharp.fluid.src}
               alt={item.node.alt}
               fluid={item.node.img.childImageSharp.fluid}
          />
          <S.ProductInfo>
            <S.TextWrap>
              <ImLocation/>
              <S.ProductTitle>{item.node.name}</S.ProductTitle>
            </S.TextWrap>
            <Button primary='true' round='true'
                    css={`position: absolute; 
                          top: 420px;
                          font-size: 14px;`}
                    to='/trips'>
              {item.node.alt}
            </Button>
          </S.ProductInfo>
        </S.ProductCart>
      )
    })
    return tripsArray
  }

  return (
    <S.ProductContainer>
      <S.ProductHeading>{heading}</S.ProductHeading>
      <S.ProductWrapper>{getTrips(data)}</S.ProductWrapper>
    </S.ProductContainer>
  );
}

export default Trips