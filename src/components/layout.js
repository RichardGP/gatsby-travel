
import * as React from "react"
import Header from "./Header/header.component"
import { GlobalStyles } from "./styles/global.styles"

const Layout = ({ children }) => {
  return (
    <>
      <GlobalStyles/>
      <Header/>
      <main>{children}</main>
    </>
  )
}
export default Layout
