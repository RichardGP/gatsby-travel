import React from "react"
import * as S from './header.styles'
import {menuData} from "../../data/menu-data"

import {Button} from "../Button/button.styles"

const Header = () => (
  <S.Nav>
    <S.NavLink to='/'>EXPLORIX</S.NavLink>
    <S.Bars/>
    <S.NavMenu>
      {menuData.map((item, index)=>(
        <S.NavLink to={item.link} key={index}>
          {item.title}
        </S.NavLink>
      ))}
    </S.NavMenu>
    <S.NavBtn>
      <Button primary='true' round='true' to='/trips'>
        Book a Flight
      </Button>
    </S.NavBtn>
  </S.Nav>
)

export default Header
