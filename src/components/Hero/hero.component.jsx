import React from "react"
import * as S from './hero.styles'
import { Button } from "../Button/button.styles"
import video from '../../assets/videos/travel.mp4'

const Hero = () => (
  <S.HeroContainer>
    <S.HeroBg>
      <S.VideoBg src={video} type='video/mp4' autoPlay
                 muted loop playInline />
    </S.HeroBg>
    <S.HeroContent>
      <S.HeroItems>
        <S.HeroH1>
          Unreal Destinations
        </S.HeroH1>
        <S.HeroP>Out of this world</S.HeroP>
        <Button primary='true' big='true'
                round='true' to='/trips'>
          Travel Now
        </Button>
      </S.HeroItems>
    </S.HeroContent>
  </S.HeroContainer>
)

export default Hero