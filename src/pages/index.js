import * as React from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"
import Hero from "../components/Hero/hero.component"
import Trips from "../components/Trips/trips.component"

const IndexPage = () => (
  <Layout>
    <Seo title="Home" />
    <Hero/>
    <Trips heading='Our Favorite Destinations'/>
  </Layout>
)

export default IndexPage
